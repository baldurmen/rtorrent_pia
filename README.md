# rTorrent recipe for PIA and ruTorrent

This repository contains configuration parameters to help set up rTorrent
along with a Private Internet Access (PIA) VPN and the ruTorrent web
interface.


## Required packages

This recipe is based on Debian stretch.

Install the required packages:

```
  apt install git openvpn rtorrent curl tmux
```

For ruTorrent, you will also need a webserver with PHP.


## Basic setup with OpenVPN and rTorrent

- First, clone this repository in a temporary directory:

```
git clone https://gitlab.com/jcharaoui/rtorrent_pia.git
cd rtorrent_pia
```


### OpenVPN

This sets up OpenVPN based on the default configuration provided by PIA with
one difference. The route-up.sh script removes the default route added by
OpenVPN after connecting, so that only applications bound to the VPN interface
pass through the VPN. All other traffic goes out through the regular network
interface.

- Copy the files under ./openvpn to /etc/openvpn
- If required, set a different PIA server in pia.conf (must be a port forward
enabled gateway)
- Put your PIA username and password in /etc/openvpn/pia_auth
- Adjust the permissions on this file: `sudo chmod 600 /etc/openvpn/pia_auth`


### rTorrent

- Create a new user account named rtorrent
- Copy ./rtorrent/.rtorrent.rc to /home/rtorrent
- Customise this rTorrent configuration file (ports, download location, etc.)


### systemd units

- Copy the unit files from ./systemd-units to /etc/systemd/system
- Reload systemd: `sudo systemctl daemon-reload`
- Enable the pia-port unit: `sudo systemctl enable pia-port.service`


## Running

You can launch rTorrent along with the OpenVPN connexion by simply running the
command below:

```
sudo systemctl start rtorrent.service
```

If you wish to start rTorrent at system start:

```
sudo systemctl enable rtorrent.service
```

Once rTorrent is running, you can attach to the rTorrent console with the
following command:

```
sudo -u rtorrent tmux attach -t rtorrent
```

To detach, simply type: Ctrl-b d


## Advanced setup with ruTorrent

If you want to set up a web interface for rTorrent, you can follow these
instructions.

- Clone https://github.com/Novik/ruTorrent.git into /var/www
- Copy the config file from ./rutorrent into /var/www/ruTorrent/conf
- Set up a webserver to serve ruTorrent. Here's an example with Apache 2.4:

```
  Alias /rutorrent /var/www/ruTorrent
  <Directory /var/www/ruTorrent>
    <FilesMatch ".+\.ph(p[3457]?|t|tml)$">
        SetHandler "proxy:unix:/run/php/php7.0-fpm-rtorrent.sock|fcgi://localhost"
    </FilesMatch>
    AuthType Basic
    AuthName "ruTorrent"
    AuthUserFile "/etc/apache2/htpasswd.rutorrent"
    Require valid-user
    Require local
  </Directory>
```

This example uses the php7.0-fpm module running as user rtorrent. The user
under which ruTorrent runs must be able to access the rtorrent socket file.

ruTorrent should also be protected behind basic authentication as the app
doesn't offer any kind of authentication. HTTPS should also be used.

- Apply patch to ruTorrent to fix check_port plugin. Since rtorrent is bound
to a non-default network interface, ruTorrent's check_port plugin will
always report that the BitTorrent port is closed. There's a pull request
upstream to fix that problem: https://github.com/Novik/ruTorrent/pull/1475
